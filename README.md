# LPaaS Server #

## Disclaimer ##
This is a graduation project by Andrea Muccioli based on the original research project [tuProlog](https://bitbucket.org/tuprologteam/tuprolog) of the University of Bologna - Alma Mater Studiorum. This repository is not the official one and its only purpose is to showcase the code written for said project.

## What is LPaaS - Smart Bathroom App ##
This Android Application was developed to provide a client-side interface in the test application scenario for [LPaaS Server](https://bitbucket.org/andre_muccio/lpaas-server/overview). The application collects data from the servers of the smart bathroom and processes it through its inner inference engine (built using tuProlog) to obtain a list of high-level warnings to show to the user.

![gitScreenshot_20170306-120917.jpg](https://bitbucket.org/repo/Lyyyg8/images/2631951511-gitScreenshot_20170306-120917.jpg) ![gitlist1.png](https://bitbucket.org/repo/nqqLan/images/2656249600-gitlist1.png)

## More Info ##
Check the [LPaaS Server](https://bitbucket.org/andre_muccio/lpaas-server/overview) for more info on the application scenario (smart bathroom).

## Installation guide ##

* Download project code
* Import project in Android Studio
* Build using gradle
* Note: the app needs at least one [LPaaS Server](https://bitbucket.org/andre_muccio/lpaas-server/overview) running to give significant result. The server URL can be set in the settings page.

## Contacts ##

* Code by: [Andrea Muccioli](mailto:andre.muccio@gmail.com)
* Official tuProlog page @ APICe: http://apice.unibo.it/xwiki/bin/view/Tuprolog/WebHome